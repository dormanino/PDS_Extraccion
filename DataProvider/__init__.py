import json
import LatestFileVersion
import inspect
from Models import PdsAGRMZDataModels
import datetime

agrmz_file = LatestFileVersion.latest_file_version('json', 'PDS_KGS_AGRMZ',
                                                   current='C:\\Users\\SubarFernanOlivera\\PycharmProjects'
                                                           '\\PDS_Extraccion\\PDS_Extractors')

# agrmz_file = LatestFileVersion.latest_file_version('json', 'PDS_KGS_AGRMZ',
#                                                       current='C:\\Users\\vravagn\\PycharmProjects'
#                                                       '\\PDS_Extraccion\\PDS_Extractors')

data_json = json.load(open(agrmz_file))
swap_list = []
swap_string = ''
counter = 0
i = 0
# adjust input proceeding correction in the base
while i < len(data_json):
    # reached a start of register
    if data_json[i]['data'][1] == '_' and counter is 0:
        # include data on swap
        swap_string += data_json[i]['data']
        # pos (39, 42)
        pos_first_register_data = data_json[i]['data'][39:42]
        # asa_ab (55, 58)
        asa_ab_first_register_data = data_json[i]['data'][55:58]
    elif data_json[i]['data'][1] == '_' and counter is not 0:
        # check if the next register has the same content from previous
        # pos (39, 42)
        pos_next_register_data = data_json[i]['data'][39:42]
        # asa_ab (55, 58)
        asa_ab_next_register_data = data_json[i]['data'][55:58]
        if pos_first_register_data == pos_next_register_data and asa_ab_first_register_data == asa_ab_next_register_data:
            counter += 1
            i += 2
            continue
        else:
            swap_list.append(swap_string)
            swap_string = ''
            # restart counter with current line data for further correct analysis
            counter = 0
            continue
    else:
        counter += 1
        swap_string += data_json[i]['data']

    # advance line if  less then allowed range
    if i < len(data_json):
        i += 1

date = datetime.date.today()
date_string = date.strftime('%y%m%d')

with open(date_string + 'PDS_KGS_AGR_test.json', 'w', encoding='utf-8') as f:
    json.dump(swap_list, f, indent=4, sort_keys=True, ensure_ascii=False)

slices = {
    'header1': {
        'abm_saa': (3, 23),
        'la': (23, 25),
        'lt': (26, 27),
        'bu_su': (29, 38),
        'pos': (39, 42),
        'hwa': (43, 47),
        'sp': (48, 50),
        'r': (51, 52),
        'p': (53, 54),
        'asa': (55, 58),
        'em_ab': (61, 67),
        'em_bis': (72, 79)
    },
    'header2': {
        'benennung': (3, 54),
        'asb': (55, 58),
        't_a': (63, 69),
        't_b': (69, 75)
    },
    'pb_header': {
        'vkfbez': (4, 55),
        'anz': (55, 58)
    },
    'bg_codebed_header': {
        'bg': (24, 25),
        'code': (30, 62)
    },
    'bg_baubarkeit_header': {
        'bg': (24, 25),
        'code': (30, 62)
    },
    'verw_st_header': {
        'bg': (24, 25),
        'code': (30, 62)
    },
    'extra_info': {
        'bg': (24, 25),
        'code': (30, 62)
    }
}
# ranges = [(int(r[0]), int(r[1])) for r in [s.split(':') for s in [slice_of_ranges.items() in [i in slices]]]]
# TODO split codes, saas and bm's in separate files
for full_line in swap_list:
    amnt_of_lines = int(len(full_line) / 80)
    data_list = []
    for line in range(1, amnt_of_lines + 1):
        end_char = line * 80
        start_char = end_char - 80
        substring = full_line[start_char:end_char + 1]
        print(substring)
        print(slices[line].values())
        for q, r in zip(slices[line].keys(), slices[line].values()):
            data = substring[r[0]:r[1]]
            if 'PB/ZUSTEUERBED' in substring:
                pass
            # TODO define amount of lines to analyse
            elif 'BG/BAUBARKEITSBED:' in substring:
                pass
            # TODO define amount of lines to analyse
            elif 'VERW.-ST.:' in substring:
                pass
            # TODO define amount of lines to analyse

        # for r in slices[line].values():
        #     if not substring[r[0]:r[1]].replace(" ", "") == '': print('not clear')

        # data = {p: [v, None][v.isspace()] for p, v in [(p, substring[r[0]:r[1]]), r in slices[line].values()]}

    params = [data_list[arg] if arg in data else None for arg in inspect.getfullargspec(PdsAGRMZDataModels).args[1:]]
    agr = PdsAGRMZDataModels(*params)
