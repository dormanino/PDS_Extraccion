import py3270


class MBBrasMainframeTN3270Connection:
    def __init__(self):
        self.emulator = py3270.Emulator(visible=True)
        self.emulator.connect('cpua')
        self.emulator.wait_for_field()
        if self.emulator.is_connected():
            if self.emulator.string_found(11, 50, 'Usuario :'):
                self.emulator.wait_for_field()
                print('Session loaded fine')
                pass
            else:
                print('Session not created! Log Off')
                self.emulator.terminate()
        else:
            self.emulator.terminate()


class LogInMBBrasTN3270(MBBrasMainframeTN3270Connection):
    def __init__(self):
        super().__init__()
        user = input('Please insert your F mainframe user ID: ')
        password = input('Please, input your user ' + user + ' password: ')
        if self.emulator.is_connected():
            self.emulator.move_to(11, 60)
            self.emulator.send_eraseEOF()
            self.emulator.send_string(user, 11, 60)
            self.emulator.move_to(12, 60)
            self.emulator.send_eraseEOF()
            self.emulator.send_string(password, 12, 60)
            self.emulator.send_enter()
            self.emulator.wait_for_field()
        if self.emulator.string_get(2, 2, 12) == "Command ===>":
            self.emulator.wait_for_field()
            print('mainframe on Command ===>')


class LogInMBBrasTN3270PDS(LogInMBBrasTN3270):

    def mainframe_connection(self):
        self.emulator.send_string('5', 2, 15)
        self.emulator.move_to(24, 80)
        self.emulator.send_enter()
        self.emulator.wait_for_field()

        if self.emulator.string_get(1, 1, 80).replace(' ', '') == '':
            self.emulator.terminate()
        else:
            self.emulator.send_string('PDS', 3, 2)
            self.emulator.move_to(24, 80)
            self.emulator.send_enter()
            self.emulator.wait_for_field()

            if self.emulator.string_get(1, 25, 4) == 'NFC:' and self.emulator.string_get(19, 32, 9) == 'PASSWORT:':
                self.emulator.send_string('154', 1, 61)
                self.emulator.send_string('PDS', 19, 43)
                self.emulator.move_to(24, 80)
                self.emulator.send_enter()
                self.emulator.wait_for_field()

                # if PDS available and ready
                if self.emulator.string_get(1, 25, 4) == 'NFC:':

                    print('PDS on you fuck')
                    return self.emulator
